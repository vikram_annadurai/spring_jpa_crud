package com.springjpatrial.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springjpatrial.model.Student;
import com.springjpatrial.service.StudentService;

@RestController
@RequestMapping("/v1/student")
public class StudentController {

	@Autowired
	StudentService studentService;

	@PostMapping("/createstudent")
	public Student create(@RequestBody Student data) {
		return studentService.create(data);
	}

	@GetMapping
	public ResponseEntity<List<Student>> getAllStudents(
			@RequestParam(defaultValue = "2") Integer pageNo,
			@RequestParam(defaultValue = "1") Integer pageSize,
			@RequestParam(defaultValue = "studentid") String sortBy) {
		List<Student> list = studentService.getAllStudents(pageNo, pageSize, sortBy);
		return new ResponseEntity<List<Student>>(list, new HttpHeaders(), HttpStatus.OK);

	}

	@GetMapping("/getallstudent")
	public List<Student> getallstudent() {
		return studentService.getall();
	}

	@GetMapping("/findStudent/{id}")
	public Student findstudent(@PathVariable("id") long studentid) {
		return studentService.findstudent(studentid);
	}

	@PutMapping("/updatestudent/{id}")
	public Optional<Object> update(@PathVariable int id, @RequestBody Student data) {
		return studentService.update(id, data);
	}

	@DeleteMapping("/deletestudent/{id}")
	public void delete(@PathVariable long id) {
		studentService.delete(id);
	}

}
