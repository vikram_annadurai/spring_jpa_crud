package com.springjpatrial.service;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.springjpatrial.dao.StudentDao;
import com.springjpatrial.model.Student;
import com.springjpatrial.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	StudentDao studentDao;

	@Autowired
	StudentRepository repository;

	public Student create(Student data) {
		return studentDao.createStudent(data);
	}

	public List<Student> getall() {
		return studentDao.getAllStudent();
	}

	public Student findstudent(long studentid) {
		return studentDao.findStudent(studentid);
	}

	public void delete(long id) {
		studentDao.deleteStudent(id);
	}

	public Optional<Object> update(long id, Student data) {
		return studentDao.updateStudent(id, data);
	}

	public List<Student> getAllStudents(Integer pageNo, Integer pageSize, String sortBy) {
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by("studentid"));
		Page<Student> pagedResult = repository.findAll(paging);

		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Student>();
		}
	}

}