package com.springjpatrial.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.springjpatrial.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> , PagingAndSortingRepository<Student, Long>{

	
	 
}