package com.springjpatrial.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springjpatrial.model.Student;
import com.springjpatrial.repository.StudentRepository;

@Component
public class StudentDao {
	
	@Autowired 
	StudentRepository student;

	public Student createStudent(Student data) {
		return (Student) student.save(data);
	}

	public List<Student> getAllStudent() {
		return student.findAll();
	}
	
	public Student findStudent(long studentid) {
		return student.findById(studentid).get();
	}

	public void deleteStudent(long id) {
		 student.deleteById(id);
	}

	public Optional<Object> updateStudent( Long id, Student data) {
		return student.findById(id).map(stu -> {
        stu.setStudentid(data.getStudentid());
        stu.setStudentname(data.getStudentname());
        stu.setStudentage(data.getStudentage());
        return student.save(stu);
    });}
	
	

}
